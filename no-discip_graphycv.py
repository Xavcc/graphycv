#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
A Graphy CV for transdisciplanary human being
by XavCC
GNU GENERAL PUBLIC LICENSE Version 3
"""

import numpy as np
import matplotlib.pyplot as plt

# set plot style
plt.style.use('seaborn-white')

# Name fields and assign value

field = ['Arts', 'Sciences', 'Hacking', 'Engineering', 'Design']
ecology = [
    72, # Arts
    78, # Sciences
    80, # Hacking
    92, # Engineering
    92, # Design
    72, # Arts
    ]
digitaltech = [
    65, # Arts
    60, # Sciences
    60, # Hacking
    55, # Engineering
    65, # Design
    65, # Arts
    ]
teaching = [
    55, # Arts
    62, # Sciences
    70, # Hacking
    82, # Engineering
    82, # Design
    55, # Arts
    ]
biodesign = [
    75, # Arts
    92, # Sciences
    95, # Hacking
    88, # Enginering
    95, # Design
    75, # Arts
    ]

# Initialise the spider plot by setting figure size and polar projection
plt.figure(figsize=(10, 6), dpi=100)
plt.subplot(polar=True)

plt.subplots_adjust(wspace=0.25, hspace=0.20, top=0.85, bottom=0.05)

theta = np.linspace(0, 2 * np.pi, len(biodesign))

# Arrange the grid into number of capacties equal parts in degrees

lines, labels = plt.thetagrids(range(0, 360, int(360 / len(field))), field)

plt.tick_params(direction='out', length=6, width=4, labelcolor='#911C14')

# Plot Ecology capacities graph & area color

plt.plot(theta, ecology, '#FE984C') # Jaffa FE984C

# Plot Digital Techn capacities graph
# Tamarillo #911C14 for Contrast ratio: 4.6:1 | AA with teaching

plt.plot(theta, digitaltech, '#911C14')

# Plot Teaching capacities graph

plt.plot(theta, teaching, '#BCC06E') # Olive Green #BCC06E

# Plot Biodesign capacities graph

plt.plot(theta, biodesign, '#2A65BC') # Denim 2A65BC
plt.fill(theta, biodesign, '#11B3D3', alpha=0.2) # Pacific Blue 11B3D3

# Add legend and title for the plot

plt.legend(labels=('Ecology', 'Digital Tech', 'Teaching', 'Biodesign'), bbox_to_anchor=(0.9, 0.4))
plt.title('My Graphy CV for transdisciplanary human being', fontsize=20, family='cmtt10',
          horizontalalignment='center', color='#2A65BC') # Denim 2A65BC call back biodesign

plt.show()
